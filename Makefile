PORTNAME=	blued
DISTVERSIONPREFIX=	v
DISTVERSION=	0.2
CATEGORIES=	sysutils
MASTER_SITES=	https://git.lysator.liu.se/kempe/blued/-/archive/v${DISTVERSION}/

MAINTAINER=	kempe@lysator.liu.se
COMMENT=	Bluetooth daemon providing pairing and HID support

LICENSE=	BSD2CLAUSE
LICENSE_FILE=	${WRKSRC}/LICENSE

LIB_DEPENDS=	libev.so:devel/libev

USES=	cmake pkgconfig

OPTIONS_DEFINE=	BLUED BLUECONTROL

OPTIONS_DEFAULT=	BLUED BLUECONTROL

BLUED_DESC=		blued Bluetooth daemon
BLUECONTROL_DESC=	CLI for controlling the daemon

# blued
BLUED_CMAKE_BOOL=	BUILD_DAEMON
BLUED_BUILD_DEPENDS=	cxxopts>0:devel/cxxopts \
			msgpack>0:devel/msgpack \
			ksql>0:devel/ksql
BLUED_LIB_DEPENDS=	libucl.so:textproc/libucl \
			libsqlite3.so:databases/sqlite3
BLUED_PLIST_FILES=	sbin/blued \
			etc/rc.d/blued \
			etc/blued.conf \
			share/examples/blued/blued.conf.example \
			share/man/man8/blued.8.gz

# bluecontrol
BLUECONTROL_CMAKE_BOOL=	BUILD_CLI
BLUECONTROL_BUILD_DEPENDS=	cxxopts>0:devel/cxxopts \
				msgpack>0:devel/msgpack
BLUECONTROL_PLIST_FILES=	bin/bluecontrol \
				share/man/man8/bluecontrol.8.gz

USERS=	blued
GROUPS= blued

.include <bsd.port.options.mk>

.if ${PORT_OPTIONS:MBLUED}
post-install:
	${MKDIR} ${STAGEDIR}${PREFIX}/etc
	${INSTALL_DATA} ${FILESDIR}/blued.conf ${STAGEDIR}${PREFIX}/etc
.endif

.include <bsd.port.mk>
